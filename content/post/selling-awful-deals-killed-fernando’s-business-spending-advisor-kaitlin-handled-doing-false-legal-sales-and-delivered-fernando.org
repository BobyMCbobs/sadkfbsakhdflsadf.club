+++
title = "Selling awful deals killed Fernando’s business. Spending advisor Kaitlin handled doing false legal sales and delivered Fernando"
author = ["Poticufurus Coffeelini"]
date = "2021-09-21"
image = "/leon-Oalh2MojUuk-unsplash.jpg"
+++
#+HUGO_BASE_DIR: ../
#+HUGO_SECTION: ./
#+HUGO_WEIGHT: auto
#+HUGO_AUTO_SET_LASTMOD: t
#+html: <br/>
#+AUTHOR: Poticufurus Coffeelini

#+html: Photo by <a href="https://unsplash.com/@myleon?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Leon</a> on <a href="https://unsplash.com/s/photos/business-meeting?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
