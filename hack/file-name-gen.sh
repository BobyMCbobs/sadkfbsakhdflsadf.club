#!/usr/bin/env bash
set -euo pipefail

TITLE=$*

echo $TITLE | tr '[:upper:]' '[:lower:]' | sed 's/[ ]/-/g' | sed "s/['\".?,\!\(\)]//g"
