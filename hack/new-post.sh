#!/usr/bin/env bash

repoRoot=$(git rev-parse --show-toplevel)
postFolder=$repoRoot/content/post
read -p 'Story: ' storyNameRaw
read -p 'Author: ' author
read -p 'Image URL: ' imageURL
if [ ! -z "$imageURL" ]; then
    read -p 'Image credit HTML: ' imageCredit
fi
storyName=$(bash $repoRoot/hack/file-name-gen.sh $storyNameRaw)
theDate=$(date +%Y-%m-%d)
postPath=$postFolder/$storyName.org
postPathBasename=$(basename $postPath)

echo -e "Writing post '$postPathBasename':\n"
cat <<EOF | tee $postPath
+++
title = "$storyNameRaw"
author = ["$author"]
date = "$theDate"
image = "/"
+++
#+HUGO_BASE_DIR: ../
#+HUGO_SECTION: ./
#+HUGO_WEIGHT: auto
#+HUGO_AUTO_SET_LASTMOD: t
#+html: <br/>
#+AUTHOR: $author

#+html: $imageCredit
EOF
